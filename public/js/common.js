/**
 * Created by gancona on 25/06/15.
 */
var cvol = cvol || {};
cvol.cookielaw = function () {
    "use strict";
    var e = {}, t = '<div id="cookieChoiceInfo" style="position: fixed; width: 100%; border-top-width: 1px; border-top-style: solid; border-top-color: #BEBEBE; margin: 0px; left: 0px; bottom: 0px; padding: 20px 0px; z-index: 1000; text-align: left; font-size: 12px; background-color: #fff;"><span style="margin-left: 25px;width: 80%;color: #333333;display: inline-block;">Per migliorare il servizio CVOL s.r.l. e i suoi partner utilizzano i cookie. Continuando la navigazione del sito ne accetti l\'utilizzo. Se desideri maggiori informazioni sui cookie e su come modificare le impostazioni del tuo browser, leggi la nostra politica in materia di cookie.</span><a id="cookieChoiceLink" href="http://www.cvol.it/info/policies/privacy.htm" target="_self" style="margin-left: 25px;width: 80%;color: #3366CC;display: inline-block;">Per saperne di piu\'</a><a id="cookieChoiceDismiss" href="#" style="margin-left: 25px;color: #FFFFFF;background: #3366CC;border-radius: 3px;height: 25px;width: 25px;line-height: 25px;text-align: center;display: block;position: absolute;right: 25px;top: 50%;margin-top: -13px;">X</a></div>';

    function i() {
        if (!!location.hostname.match(/panel\.[a-z]+\.it/)) {
            return false
        }
        return!document.cookie.match(new RegExp("displayCookieConsent=([^;]+)"))
    }

    function a() {
        var e = window.location.hostname, t = e.split("."), i, a = new Date;
        a.setFullYear(a.getFullYear() + 1);
        t = t.slice(-2);
        i = "." + t.join(".");
        document.cookie = "displayCookieConsent=y; expires=" + a.toGMTString() + "; path=/" + "; domain=" + i;
        $("#cookieChoiceInfo").hide()
    }

    e.init = function () {
        if (i()) {
            $("body").append(t);
            $("#cookieChoiceDismiss").click(function () {
                a()
            })
        }
    };
    return e
}();

function checkPIVA(e) {
    "use strict";
    var t = 0, i, a, n = "0123456789";
    if (e === "") {
        return""
    }
    if (e.length !== 11) {
        return"La lunghezza della partita IVA non &egrave; corretta: \nla partita IVA dovrebbe essere lunga esattamente 11 caratteri."
    }
    for (a = 0; a < 11; a += 1) {
        if (n.indexOf(e.charAt(a)) === -1) {
            return"La partita IVA contiene un carattere non valido.\nI caratteri validi sono le cifre numeriche."
        }
    }
    for (a = 0; a <= 9; a += 2) {
        t += e.charCodeAt(a) - "0".charCodeAt(0)
    }
    for (a = 1; a <= 9; a += 2) {
        i = 2 * (e.charCodeAt(a) - "0".charCodeAt(0));
        if (i > 9) {
            i = i - 9
        }
        t += i
    }
    if ((10 - t % 10) % 10 !== e.charCodeAt(10) - "0".charCodeAt(0)) {
        return"La partita IVA non &egrave; valida:\nil codice di controllo non corrisponde."
    }
    return""
}
function checkEmail(e) {
    var t = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return t.test(e)
}
function checkPassword(e) {
    return/[a-zA-Z]/g.test(e) && /[0-9]/g.test(e) && e.length >= 5
}
function isInArray(e, t) {
    for (var i in t) {
        if (t[i].split(":")[0] == e)return true
    }
    return false
}
function setCookie(e, t, i, a) {
    var n = "";
    if (i != null) {
        if (i < 0) {
            i = 3e3
        }
        var r = new Date;
        r.setTime(r.getTime() + i * 24 * 60 * 60 * 1e3);
        var n = "; expires=" + r.toGMTString()
    }
    if (t != null && t != "") {
        var o = e + "=" + t + n + "; path=/";
        if (typeof a != "undefined" && a != null && a != "" && a != false)o += "; domain=" + a;
        document.cookie = o;
        return true
    }
    return false
}
function getCookie(e) {
    var t = document.cookie;
    var i = t.split(";");
    for (var a = 0; a < i.length; a++) {
        var n = i[a].split("=");
        var r = n[0].replace(" ", "");
        if (r == e) {
            return unescape(n[1])
        }
    }
    return null
}
function getRawCookie(e) {
    var t = "" + document.cookie;
    var i = t.indexOf(e);
    if (i == -1 || e == "")return"";
    var a = t.indexOf(";", i);
    if (a == -1)a = t.length;
    return unescape(t.substring(i + e.length + 1, a))
}

function showField(e, t, i) {
    var a = document.getElementById(e);
    if (a) {
        a.style.display = t
    } else {
        return false
    }
    if (typeof i === "undefined" || i === false) {
        return
    } else {
        a.innerHTML = i
    }
}
function scrollToTop() {
    window.scrollTo(0, 0)
}
function scrollToBottom() {
    window.scrollTo(0, 4e3)
}
function scrollToObject(e) {
    var t = 0;
    var i = 0;
    while (e) {
        t += e.offsetLeft;
        i += e.offsetTop;
        e = e.offsetParent
    }
    if (typeof document.body.leftMargin != "undefined") {
        t += document.body.leftMargin
    }
    if (typeof document.body.topMargin != "undefined") {
        i += document.body.topMargin
    }
    window.scrollTo(0, i)
}
var focused = false;
function scrollToError(e) {
    if (focused)return;
    var t = document.getElementById(e);
    scrollToObject(t);
    if (document.getElementById(e))document.getElementById(e).focus();
    focused = true
}
function setFocus(e) {
    document.getElementById(e).focus()
}
function setChecked(e, t) {
    var i = document.getElementById(e);
    if (i == null)return;
    i.checked = t
}
function setValue(e, t) {
    var i = document.getElementById(e);
    if (i == null)return;
    i.value = t
}
var newWin;
function popUp(e, t, i) {
    newWin = window.open(e, t, i);
    newWin.focus()
}
function enable_field(e) {
    var t = document.getElementById(e);
    if (t == null)return;
    if (t.disabled)t.disabled = false
}
function enable_fields(e) {
    var t = document.getElementsByName(e);
    if (t.length == 0)return;
    for (i = 0; i < t.length; i++) {
        if (t[i].disabled)t[i].disabled = false
    }
}
function disable_field(e) {
    var t = document.getElementById(e);
    if (t == null)return;
    if (!t.disabled) {
        t.value = "";
        t.disabled = true
    }
}
function disable_fields(e) {
    var t = document.getElementsByName(e);
    if (t.length == 0)return;
    for (i = 0; i < t.length; i++) {
        if (!t[i].disabled) {
            t[i].disabled = true
        }
    }
}
function maxlength(e, t, i) {
    if (!e)e = window.event;
    if (e.which) {
        var a = e.which;
        var n = false
    } else {
        var a = e.keyCode;
        var n = true
    }
    x = t.value.length;
    if (x > i) {
        t.value = t.value.substr(0, i);
        x = i
    }
    if (a == 0 && n) {
        var r = document.selection.createRange();
        var o = i - x + r.text.length;
        var s = window.clipboardData.getData("Text").substr(0, o);
        r.text = s
    } else if (x == i && (a != 8 && a != 46)) {
        return false
    }
    return true
}
function findPosX(e, t) {
    var i = 0;
    var a = e.clientWidth;
    if (e.offsetParent) {
        while (e.offsetParent) {
            i += e.offsetLeft;
            e = e.offsetParent
        }
    } else if (e.x)i += e.x;
    return i + (t ? a : 0)
}
function findPosY(e, t) {
    var i = 0;
    var a = e.clientHeight;
    if (e.offsetParent) {
        while (e.offsetParent) {
            i += e.offsetTop;
            e = e.offsetParent
        }
    } else if (e.y)i += e.y;
    return i + (t ? a : 0)
}
function progressBar(e) {
    document.write('<span id="loading" class="progressBar">' + e + '<span id="loading_dots"></span></span>')
}
function startProgressBar(e) {
    var t = "";
    e %= 4;
    for (var i = 0; i < e; i++)t += ".";
    document.getElementById("loading_dots").innerHTML = t;
    e++;
    setTimeout("startProgressBar(" + e + ")", 500)
}

function resizeImage(e, t, i, a) {
    if (!i) {
        i = new Image;
        i.src = t
    }
    if (i.width == 0) {
        i.onload = setTimeout(function () {
            resizeImage(e, t, i, a)
        }, 0);
        return
    }
    e.src = i.src;
    if (a && i.width > 600) {
        var n = (i.width - 600) / i.width;
        e.height = i.height * (1 - n);
        e.width = 600
    } else {
        e.width = i.width;
        e.height = i.height
    }
}

function getElementsByClassName(e, t, i) {
    var a = t == "*" && e.all ? e.all : e.getElementsByTagName(t);
    var n = [];
    i = i.replace(/\-/g, "\\-");
    var r = new RegExp("(^|\\s)" + i + "(\\s|$)");
    var o;
    for (var s = 0; s < a.length; s += 1) {
        o = a[s];
        if (r.test(o.className)) {
            n[n.length] = o
        }
    }
    return n
}


function checkMispelledDomain(e, t, i) {
    "use strict";
    if (!e) {
        return true
    }
    var a = ["libero.it", "hotmail.it", "yahoo.it", "alice.it", "tiscali.it", "gmail.com", "hotmail.com", "virgilio.it", "live.it", "yahoo.com", "tin.it", "fastwebnet.it", "email.it", "inwind.it", "tele2.it", "teletu.it", "interfree.it", "msn.com", "katamail.com", "yahoo.fr", "tiscalinet.it", "poste.it", "vodafone.it", "aliceposta.it", "live.com", "jumpy.it", "ymail.com", "googlemail.com", "infinito.it", "tim.it", "iol.it", "excite.it", "supereva.it", "mail.ru", "fastwebmail.it", "hotmail.fr", "freemail.it", "cheapnet.it", "aol.it", "tecnocasa.it", "postemobile.it", "me.com"];
    var n = e.indexOf("@");
    var r = t || "warn_msg_email";
    var o = i || "err_msg_email";
    var s = document.getElementById(t);
    var l = document.getElementById(i);
    hideElem(s);
    hideElem(l);
    if (n <= 0) {
        l.innerHTML = "L'indirizzo email non e' corretto. Ricontrollalo";
        showElem(o, false);
        return false
    }
    var d = e.substring(n + 1, e.length);
    for (var c in a) {
        if (a[c] === d) {
            return true
        }
    }
    if (d === "gmail.it") {
        s.innerHTML = "Avviso: forse volevi inserire '" + e.substring(0, n + 1) + "gmail.com'?<br> Verifica se la tua email &egrave; corretta.";
        showElem("warn_msg_email", false);
        return false
    }
    for (var u in a) {
        if (a.hasOwnProperty(u)) {
            var f = levenshtein(a[u], d);
            if (f > 0 && f <= 2) {
                s.innerHTML = "Avviso: forse volevi inserire '" + e.substring(0, n + 1) + a[u] + "'?<br> Verifica se la tua email &egrave; corretta.";
                showElem(r, false);
                return false
            }
        }
    }
}
function include_script(e, t) {
    "use strict";
    var i = document.createElement("script");
    i.type = "text/javascript";
    i.src = e;
    if (t) {
        i.onload = t;
        i.onreadystatechange = function () {
            if (this.readyState === 'complete"' || this.readyState === "loaded") {
                this.onload();
                this.onload = this.onreadystatechange = null
            }
        }
    }
    document.getElementsByTagName("head")[0].appendChild(i)
}
function OnloadExecute() {
    if (!window.onload_queue || window.onload_queue.length == 0)return;
    for (var e = 0; e < window.onload_queue.length; e++) {
        window.onload_queue[e]()
    }
}
function Onload(e, t) {
    if (typeof e != "function")return;
    var i = window.onload;
    window.onload = function () {
        if (i)i();
        e()
    }
}
function popupInfo(e, t, i, a) {
    w = t ? t : 600;
    h = i ? i : 400;
    x = (screen.width - w) / 2;
    y = (screen.height - h) / 2;
    window.open(e, "popup", "width=" + w + ",height=" + h + ",left=" + x + ",top=" + y + ",scrollbars=" + (a ? "yes" : "no") + ",menubar=no,status=no,location=no,resizable=no")
}

String.prototype.wordWrap = function (e, t, i) {
    var a, n, r, o, s;
    if (e < 1)return this;
    for (a = -1, r = (s = this.split("\n")).length; ++a < r; s[a] += o)for (o = s[a], s[a] = ""; o.length > e; s[a] += o.slice(0, n) + ((o = o.slice(n)).length ? t : ""))n = i == 2 || (n = o.slice(0, e + 1).match(/\S*(\s)?$/))[1] ? e : n.input.length - n[0].length || i == 1 && e || n.input.length + (n = o.slice(e).match(/^\S*/)).input.length;
    return s.join("\n")
};


function escape_component(e) {
    return escape(e).replace("+", "%2b")
}
String.prototype.wordWrap = function (e, t, i) {
    var a, n, r, o, s;
    if (e < 1)return this;
    for (a = -1, r = (s = this.split("\n")).length; ++a < r; s[a] += o)for (o = s[a], s[a] = ""; o.length > e; s[a] += o.slice(0, n) + ((o = o.slice(n)).length ? t : ""))n = i == 2 || (n = o.slice(0, e + 1).match(/\S*(\s)?$/))[1] ? e : n.input.length - n[0].length || i == 1 && e || n.input.length + (n = o.slice(e).match(/^\S*/)).input.length;
    return s.join("\n")
};
function textCounter(e, t, i) {
    if (e.value.length > i)e.value = e.value.substring(0, i); else {
        t.value = i - e.value.length
    }
}
function toggleVisible(e) {
    try {
        elm = document.getElementById(e);
        if (elm.style.display == "none")elm.style.display = "block"; else elm.style.display = "none"
    } catch (t) {
    }
}

function queryString(e) {
    var t = new PageQuery(window.location.search);
    return unescape(t.getValue(e))
}
function fbs_click() {
    u = location.href;
    t = document.title;
    window.open("http://www.facebook.com/sharer.php?u=" + encodeURIComponent(u) + "&t=" + encodeURIComponent(t), "sharer", "toolbar=0,status=0,width=626,height=436");
    return false
}
var adsense_weights = {0: {b: 1.69, d: 1.75}, 1: {a: 2.86, c: 2.68, b: 2.27, e: 3.46, d: 1.96}, 2: {a: 1.06, c: .95, b: .83, e: .94, d: .79}, 3: {a: .34, c: .31, b: .29, e: .14, d: .27}, 4: {a: 3.26, c: 2.77, b: 2.32, e: .79, d: 2.36}, 5: {a: 2.16, c: 2.39, b: 1.66, e: 1.37, d: 1.58}, 6: {a: 1.33, c: 1.51, b: .85, e: .83, d: .74}, 7: {a: 2.56, c: 2.47, b: .72, e: 1.1, d: .63}, 8: {a: 4.88, c: 4.07, b: 2.83, e: 2.32, d: 2.36}, 9: {a: 4.3, c: 6.61, b: 4.7, e: .27, d: 4.64}, 10: {a: 4.5, c: 4.43, b: 4.95, e: .31, d: 4.84}, 11: {a: 3.1, c: 3.6, b: 2.88, e: .29, d: 3.29}, 12: {a: 2.39, c: 2.93, b: 1.96, e: .97, d: 2.43}, 13: {a: 3.51, c: 4.64, b: 4.12, e: .34, d: 5.47}, 14: {a: 2.5, c: 2.86, b: 2.29, e: 1.01, d: 2.43}, 15: {a: 10.76, c: 7.4, b: 10.08, e: .74, d: 9.38}, 16: {a: 3.37, c: 3.47, b: 3.11, e: 1.37, d: 2.93}, 17: {a: 5.18, c: 5.72, b: 4.93, e: 1.21, d: 5.65}, 18: {a: 4.05, c: 6.19, b: 3.89, e: 2.63, d: 4.48}, 19: {a: 6.07, c: 11.77, b: 9.36, e: 1.24, d: 7.22}, 20: {a: 5.31, c: 5.74, b: 5.36, e: .23, d: 5.89}, 21: {a: 3.87, c: 4.14, b: 2.61, e: .79, d: 2.47}, 22: {a: 1.08, c: 1.1, b: .77, e: .27, d: .86}, 23: {a: .99, c: .92, b: .65, e: .16, d: .67}, 24: {a: 12.15, c: 11.21, b: 10.91, e: 3.71, d: 9.61}, 25: {a: 7.72, c: 7.54, b: 7.4, e: .36, d: 7.52}, 26: {a: 8.77, c: 9.2, b: 6.82, e: 2.5, d: 6.73}, 27: {a: 20.45, c: 41.98, b: 28.82, e: 1.49, d: 28.64}, 28: {a: 32.13, c: 32.04, b: 34.2, e: .4, d: 34.94}, 29: {a: 2.23, c: 2.27, b: .47, e: 1.1, d: .63}, 30: {a: 4.43, c: 3.62, b: 1.57, e: 1.28, d: 1.04}, 31: {a: 11.63, c: 13.54, b: 3.76, e: 3.51, d: 3.85}, 32: {a: 4.52, c: 3.38, b: 2.83, e: 2.2, d: 1.4}, 33: {a: 6.84, c: 8.62, b: 4.19, e: 3.11, d: 3.17}, 34: {a: 1.13, c: 1.26, b: .97, e: .4, d: 1.06}, 35: {a: 1, c: 1, b: 1, e: 1, d: 2}, 36: {a: 1.01, c: .94, b: .65, e: .52, d: .72}, 37: {a: 9.54, c: 8.62, b: 7.78, e: 2.77, d: 8.66}, 38: {a: 15.88, c: 16.11, b: 5.89, e: 4.93, d: 6.62}, 39: {a: 3.98, c: 2.74, b: 1.49, e: 1.06, d: 2.03}, 40: {a: 4.1, c: 2.75, b: 2.75, e: .52, d: 3.49}, 41: {a: 1.24, c: 1.53, b: .94, e: .16, d: 1.22}, 42: {a: 6.93, c: 6.82, b: 7, e: 1.87, d: 6.95}, 43: {a: 3.69, c: 3.24, b: 1.42, e: 1.94, d: 1.44}, 44: {a: 2.12, c: 2.5, b: 1.62, e: .32, d: 2.93}, 50: {a: 8.77, c: 9.2, b: 6.82, e: 2.5, d: 6.73}};
function jc_toggle_all(e) {
    var t = document.forms[0].elements;
    for (var i = 0; i < t.length; i++) {
        if (t[i].name == "jc" && t[i].id != "jc_all") {
            t[i].checked = e.checked
        }
    }
}

function hasClass(e, t) {
    return $(e).hasClass(t)
}
function addClass(e, t) {
    if ($(e).hasClass(t)) {
        return
    }
    $(e).addClass(t)
}
function removeClass(e, t) {
    $(e).removeClass(t)
}
function push_button(e, t) {
    if (t)addClass(e, "gray push2"); else addClass(e, "push")
}
function release_button(e, t) {
    if (t)removeClass(e, "gray push2"); else removeClass(e, "push")
}

function in_array(e, t) {
    var i = t.length;
    for (var a = 0; a < i; a++) {
        if (t[a] == e)return true
    }
    return false
}

function parse_url(e, t) {
    var i = RegExp("^(([^:/?#]+):)?(//([^/?#]*))?([^?#]*)(\\?([^#]*))?(#(.*))?");
    var a = e.match(i);
    if (t && typeof t == "number" && Number(t) == t) {
        return a[t]
    }
    return{scheme: a[2], authority: a[4], path: a[5], query: a[7], fragment: a[9]}
}

function formatPrice(item) {
    //var e = $("#price");
    if (item.length === 0) {
        return
    }
    item.val(number_clean(item.val()));
    item.val(item.val().replace(/\B(?=(\d{3})+(?!\d))/g, "."))
    console.log("Prezzo: " + item);
}

function currencyFormatIT (num) {
    return num
        .toFixed(2) // always two decimal digits
        .replace(".", ",") // replace decimal point character with ,
        .replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.") + " €" // use . as a separator
}

function number_clean(e) {
    e = e.replace(/[^0-9]/g, "");
    return e
}