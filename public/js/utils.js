


function changeCarbrand(e, t, i) {
    select_carmodel = document.f.cm;
    select_carmodel.options.length = 0;
    if (e) {
        select_carmodel.disabled = false;
        xmlhttp = ajaxFunction();
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4) {
                models = xmlhttp.responseText.replace(/<.*>/g, "");
                models = models.split("~");
                models.sort();
                populateCarmodels(models, t, i)
            }
        };
        xmlhttp.open("GET", "/templates/common/carmetamodels.html?cb=" + e, true);
        xmlhttp.send(null)
    } else {
        select_carmodel.options[0] = new Option("Scegli prima la marca", "", false, false);
        select_carmodel.options[0].selected = true;
        select_carmodel.disabled = true
    }
}
function populateCarmodels(e, t, s) {
    select = document.f.cm;
    select.options[0] = new Option("Scegli il modello", "", false, false);
    for (i = 0; i < e.length; i++) {
        if (e[i].indexOf("|") > 0) {
            model = e[i].split("|");
            if (model[0] && model[1]) {
                select.options[i] = new Option(model[0], model[1], false, false);
                if (model[1] == t)
                    select.options[i].selected = true
            }
        }
    }
    select.options[e.length] = new Option("Altro modello", "000000", false, false);
    if (t == "000000")
        select.options[e.length].selected = true
}

function SearchFeat() {
    var e = "";
    var t = Array();
    t[0] = "ps";
    t[1] = "pe";
    t[2] = "ms";
    t[3] = "me";
    t[4] = "rs";
    t[5] = "re";
    t[6] = "lns";
    t[7] = "lne";
    var i = "";
    for (var s = 0; s < t.length; s++) {
        i += t[s] + ":" + document.getElementById(t[s]).value + "&"
    }
    if (document.f.st) {
        for (var n = 0; n < document.f.st.length; n++) {
            if (document.f.st[n].checked == "1") {
                e = document.f.st[n].value
            }
        }
        if (e) {
            i += "st:" + e
        }
    }
    setTimeout("document.cookie='features=" + i + "'", 0)
}