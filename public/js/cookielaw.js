var cvol = cvol || {};

cvol.cookielaw = (function() {
    'use strict';

    var exports = {},
        displayElement = '<div id="cookieChoiceInfo" style="position: fixed; width: 100%; border-top-width: 1px; border-top-style: solid; border-top-color: #BEBEBE; margin: 0px; left: 0px; bottom: 0px; padding: 20px 0px; z-index: 1000; text-align: left; font-size: 12px; background-color: #fff;"><span style="margin-left: 25px;width: 80%;color: #333333;display: inline-block;">Per migliorare il servizio CVOL s.r.l. e i suoi partner utilizzano i cookie. Continuando la navigazione del sito ne accetti l\'utilizzo. Se desideri maggiori informazioni sui cookie e su come modificare le impostazioni del tuo browser, leggi la nostra politica in materia di cookie.</span><a id="cookieChoiceLink" href="http://www.cvol.it/info/policies/privacy.htm" target="_self" style="margin-left: 25px;width: 80%;color: #3366CC;display: inline-block;">Per saperne di piu\'</a><a id="cookieChoiceDismiss" href="#" style="margin-left: 25px;color: #ffffff;background: #71c834;border-radius: 3px;height: 25px;width: 50px;line-height: 25px;text-align: center;display: block;position: absolute;right: 25px;top: 50%;margin-top: -13px;">Chiudi</a></div>';

    function _showCookiesAccepted(){
        if(!(!location.hostname.match(/panel\.[a-z]+\.it/))){
            return false;
        };
        return !document.cookie.match(new RegExp('displayCookieConsent=([^;]+)'));
    }

    function _acceptCookies(){

        var hostname = window.location.hostname,
            hostnameParts = hostname.split('.'),
            currentDomain,
            expiredate = new Date();

        expiredate.setFullYear(expiredate.getFullYear() + 1);
        hostnameParts = hostnameParts.slice(-2);
        currentDomain = '.' + hostnameParts.join('.');
        document.cookie = 'displayCookieConsent=y; expires=' + expiredate.toGMTString() + '; path=/' + '; domain=' + currentDomain;
        $('#cookieChoiceInfo').hide()

    }
    exports.init = function() {
        if(_showCookiesAccepted()){
            $('body').append(displayElement);
            $('#cookieChoiceDismiss').click(function(){
                _acceptCookies();
            });
        }
    };

    return exports;
}());

$(function(){
    cvol.cookielaw.init();
});
