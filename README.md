# CVOL first release

This is the first release of CVOL webapp

## Requirements

* [NodeJs](http://nodejs.org)
* [mongodb](http://mongodb.org)
* [imagemagick](http://www.imagemagick.org/script/index.php)

## Install

```sh
$ git clone git@bitbucket.org:giuseppeancona/cvol.git
$ npm install
```

**NOTE:** Do not forget to set the facebook, twitter, google, linkedin and github `CLIENT_ID`s and `SECRET`s. 

Don't forget to set env variables for the imager config.

(Note: When you do npm start, `NODE_PATH` variable is set from package.json start script. If you are doing `node server.js`, you need to make sure to set this)

Then visit [http://localhost:3000/](http://localhost:3000/)

## Tests

```sh
$ npm test
```