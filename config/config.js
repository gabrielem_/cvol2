/*!
 * ContoVenditaOnLine
 * Copyright(c) 2015 INNAAS Srl
 * http://www.innaas.com
 */
/**
 * Module dependencies.
 */

var path = require('path');
var extend = require('util')._extend;

var development = require('./env/development');
var test = require('./env/test');
var production = require('./env/production');

var notifier = {
  service: 'sendgrid',
  APN: false,
  email: true, // true
  actions: ['comment','users','partners','usersForgot','usersConfirmPasswordChange'],
  tplPath: require('path').resolve(__dirname, '../app/mailer/templates'),
  key: 'Incontro2015',
  parseApiKey:'Incontro2015',
  parseAppId:'cvol-web',
  tplType: 'html',
  sendgridUser:'cvol_web'
};

var defaults = {
  root: path.normalize(__dirname + '/..'),
  notifier: notifier
};

/**
 * Expose
 */

module.exports = {
  development: extend(development, defaults),
  test: extend(test, defaults),
  production: extend(production, defaults)
}[process.env.NODE_ENV || 'development'];
