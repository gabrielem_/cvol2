/*!
 * ContoVenditaOnLine
 * Copyright(c) 2015 INNAAS Srl
 * http://www.innaas.com
 */
/*!
 * Module dependencies.
 */

var mongoose = require('mongoose');
var LocalStrategy = require('passport-local').Strategy;
var User = mongoose.model('User');
var Partner = mongoose.model('Partner');

var local = require('./passport/local');
var google = require('./passport/google');
var facebook = require('./passport/facebook');
var twitter = require('./passport/twitter');


/**
 * Expose
 */

module.exports = function (passport, config) {
  // serialize sessions



    passport.serializeUser(function(user, done) {
        done(null, user.id)
    })



    passport.serializeUser(function(partner, done) {
        done(null, partner.id)
    })

    passport.deserializeUser(function(id, done) {
        User.load({ criteria: { _id: id } }, function (err, user) {
            if (err) done(err);
            if (user) {
                done(null, user);
            } else {
                Partner.load({ criteria: { _id: id } }, function (err, partner) {
                    if (err) done(err);
                    done(null, partner);
                })
            }
        })
    });





  // use these strategies
  passport.use(local);
  passport.use(google);
  passport.use(facebook);
  passport.use(twitter);


    passport.use('user-local', new LocalStrategy({
            usernameField: 'email',
            passwordField: 'password' // this is the virtual field on the model
        },
        function(email, password, done) {
            User.findOne({
                email: email
            }, function(err, user) {
                if (err) return done(err);

                if (!user) {
                    return done(null, false, {
                        message: 'This email is not registered.'
                    });
                }
                if (!user.authenticate(password)) {
                    return done(null, false, {
                        message: 'This password is not correct.'
                    });
                }
                return done(null, user);
            });
        }
    ));

    // add other strategies for more authentication flexibility
    passport.use('partner-local', new LocalStrategy({
            usernameField: 'email',
            passwordField: 'password' // this is the virtual field on the model
        },
        function(email, password, done) {
            var options = {
                criteria: { email: email },
                select: 'name username email hashed_password salt active'
            };
            isUser = false;
            Partner.load(options, function(err, partner) {
                if (err) return done(err);

                if (!partner) {
                    return done(null, false, {
                        message: 'This email/username is not registered.'
                    });
                }
                if (!partner.authenticate(password)) {
                    console.log("Wrong password");
                    return done(null, false, {
                        message: 'This password is not correct.'
                    });
                }
                return done(null, partner);
            });
        }
    ));


};
