/*!
 * ContoVenditaOnLine
 * Copyright(c) 2015 INNAAS Srl
 * http://www.innaas.com
 */
/*!
 * Module dependencies.
 */

// Note: We can require users, articles and other cotrollers because we have
// set the NODE_PATH to be ./app/controllers (package.json # scripts # start)
var home = require('home');
var users = require('users');
var articles = require('articles');
var comments = require('comments');
var tags = require('tags');
var auth = require('./middlewares/authorization');
var partners = require('partners');
var recaptcha = require('node-no-captcha');
var secret = '6LcTCQgTAAAAADmSPVbA8HvbNVwUtgv117NpI6uk';
/**
 * Route middlewares
 */

var articleAuth = [auth.requiresLogin, auth.article.hasAuthorization];
var commentAuth = [auth.requiresLogin, auth.comment.hasAuthorization];
var profileAuth = [auth.requiresLogin, auth.partner.hasAuthorization];
var userProfileAuth = [auth.requiresLogin, auth.user.hasAuthorization];

/**
 * Expose routes
 */

module.exports = function (app, passport) {

  // user routes
  app.get('/login', users.login);
  app.get('/signup', users.signup);
  app.get('/logout', users.logout);
  app.post('/users',recaptcha.verify(secret) , users.create);
  app.get('/activation', users.activation);
  app.get('/users/forgot', users.forgot);
  app.get('/users/reset/:token', users.resetPassword);
  app.post('/users/reset/:token', users.resetPasswordAction);
  app.post('/users/session',
    passport.authenticate('local', {
      failureRedirect: '/login',
      failureFlash: 'Email o password non validi'
    }), users.session);
  app.get('/users/:userId', userProfileAuth, users.show);
  app.get('/auth/facebook',
    passport.authenticate('facebook', {
      scope: [ 'email', 'user_about_me'],
      failureRedirect: '/login'
    }), users.signin);
  app.get('/auth/facebook/callback',
    passport.authenticate('facebook', {
      failureRedirect: '/login'
    }), users.authCallback);
  app.get('/auth/twitter',
    passport.authenticate('twitter', {
      failureRedirect: '/login'
    }), users.signin);
  app.get('/auth/twitter/callback',
    passport.authenticate('twitter', {
      failureRedirect: '/login'
    }), users.authCallback);
  app.get('/auth/google',
    passport.authenticate('google', {
      failureRedirect: '/login',
      scope: [
        'https://www.googleapis.com/auth/userinfo.profile',
        'https://www.googleapis.com/auth/userinfo.email'
      ]
    }), users.signin);
  app.get('/auth/google/callback',
    passport.authenticate('google', {
      failureRedirect: '/login'
    }), users.authCallback);

  app.param('userId', users.load);
  app.get('/users/activation', users.activation);
  app.post('/users/activate', users.activate);
  app.post('/users/sendMessage', users.sendMessage);
  app.post('/users/forgotMail', users.forgotMail);


 // Partner routes
    app.get('/partners/listNewArticles', auth.requiresLogin, articles.partnersListNewArticles);
    app.get('/partners/signup', partners.signup);
    app.post('/partners', partners.create);
    app.get('/partners/login', partners.login);
    app.get('/logout', partners.logout);
    app.get('/partners/:partnerId', profileAuth, partners.show);

    app.post('/partners/session',
        passport.authenticate('partner-local', {
            failureRedirect: '/partners/login',
            failureFlash: 'Email o password non validi'
        }), partners.session);
    app.param('partnerId', partners.load);



  // article routes
  app.param('id', articles.load);
  app.get('/articles', articles.index);
  app.get('/articles/search', articles.query);
    // populate listbox calls
    app.get('/articles/test', articles.loadModels);
    app.get('/articles/allest', articles.loadCar);
    app.get('/articles/modelData', articles.loadData);
    app.get('/articles/modelTechData', articles.loadTechData);
    //
  app.get('/articles/new', auth.requiresLogin, auth.requiresActivation,  articles.new);
  app.post('/articles', auth.requiresLogin, articles.create);
  app.get('/articles/:id', articles.show);
  app.get('/articles/:id/edit', articleAuth, articles.edit);
  app.put('/articles/:id', articleAuth, articles.update);
  app.delete('/articles/:id', articleAuth, articles.destroy);
  app.post('/articles/:id/img', articles.removeImage);
  // home route
  app.get('/', home.index);

  // comment routes
  app.param('commentId', comments.load);
  app.post('/articles/:id/comments', auth.requiresLogin, comments.create);
  app.get('/articles/:id/comments', auth.requiresLogin, comments.create);
  app.delete('/articles/:id/comments/:commentId', commentAuth, comments.destroy);

  // tag routes
  app.get('/tags/:tag', tags.index);


  /**
   * Error handling
   */

  app.use(function (err, req, res, next) {
    // treat as 404
    if (err.message
      && (~err.message.indexOf('not found')
      || (~err.message.indexOf('Cast to ObjectId failed')))) {
      return next();
    }
    console.error(err.stack);
    // error page
    res.status(500).render('500', { error: err.stack });
  });

  // assume 404 since no middleware responded
  app.use(function (req, res, next) {
    res.status(404).render('404', {
      url: req.originalUrl,
      error: 'Not found'
    });
  });
}
