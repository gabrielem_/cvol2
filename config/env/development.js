/*!
 * ContoVenditaOnLine
 * Copyright(c) 2015 INNAAS Srl
 * http://www.innaas.com
 */
/*!
 * Module dependencies.
 */

var fs = require('fs');
var env = {};
var envFile = __dirname + '/env.json';

// Read env.json file, if it exists, load the id's and secrets from that
// Note that this is only in the development env
// it is not safe to store id's in files

if (fs.existsSync(envFile)) {
  env = fs.readFileSync(envFile, 'utf-8');
  env = JSON.parse(env);
  Object.keys(env).forEach(function (key) {
    process.env[key] = env[key];
  });
}

/**
 * Expose
 */

module.exports = {
  db: 'mongodb://localhost/cvol_dev',
  facebook: {
    clientID: process.env.FACEBOOK_CLIENTID,
    clientSecret: process.env.FACEBOOK_SECRET,
    callbackURL: "/auth/facebook/callback"
  },
  twitter: {
    clientID: process.env.TWITTER_CLIENTID,
    clientSecret: process.env.TWITTER_SECRET,
    callbackURL: "/auth/twitter/callback"
  },
  google: {
    clientID: "464938911689-f1rhpkcjv4k85c1jtbpg879hh5f5ap0t.apps.googleusercontent.com", //process.env.GOOGLE_CLIENTID,
    clientSecret: "--z3LqcMo3s6K7CAOV7DVmS7",//process.env.GOOGLE_SECRET,
    callbackURL: "/auth/google/callback" //process.env.GOOGLE_URL
  }
};
