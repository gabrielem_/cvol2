/*!
 * ContoVenditaOnLine
 * Copyright(c) 2015 INNAAS Srl
 * http://www.innaas.com
 */
/**
 * Expose
 */

module.exports = {
  variants: {
    article: {

        thumb: {        // preset
            options: {    // preset options
                pool: 5,
                scale: { width: 200, height: 150, type: 'contain' },
                crop: { width: 200, height: 150, x: 0, y: 0 },
                format: 'png',
                rotate: 'auto'
            }
        },
        large: {
            original: true  // upload original image without image processing
        },

      resize: {
          mini : '300x200',
          preview: '800x600'
      },
      crop: {
          thumb: '200x200'
      },
      resizeAndCrop: {
          large: {
              resize: '1000x1000',
              crop: '900x900'
          }
      },

    gallery: {
        crop: {
            thumb: '100x100'
            }
        }
    },partner : {
          resize: {
              mini : '300x200',
              preview: '800x600'
          },
          crop: {
              thumb: '200x200'
          }
      }

  },

  storage: {
    S3: {
   //   key: process.env.IMAGER_S3_KEY,
   //   secret: process.env.IMAGER_S3_SECRET,
   //   bucket: process.env.IMAGER_S3_BUCKET
        provider: 'amazon',
        //keyId : AKIAIWBRGYADOF7L3ZWQ,
        //key : L9xP2c/lJV9ZtTJBtY41Jl9k4662DNQBlDq2YsfE,
        key : "UzxpLZ4iaklVnq2sxrmhrfAl91ryh7rjsoxRPwFP",
        keyId: "AKIAJPCLQY32YA7BJAZQ",
        container : 'cvol2',
        region : 'eu-west-1',
        cdnUri : 'http://cvol2.s3-website-eu-west-1.amazonaws.com'
    },
    local: {
        provider: 'local',
        path: '/Users/gancona/Documents/CVOL/images/upload',
        mode: 0777
    },
      uploadDirectory : 'images/'
  },

  debug: true
}