/*!
 * ContoVenditaOnLine
 * Copyright(c) 2015 INNAAS Srl
 * http://www.innaas.com
 */
/**
 * Module dependencies.
 */


var mongoose = require('mongoose');
var LocalStrategy = require('passport-local').Strategy;
var config = require('config');
var Partner = mongoose.model('Partner');


/**
 * Expose
 */

module.exports = new LocalStrategy({
        usernameField: 'email',
        passwordField: 'password' // this is the virtual field on the model
    },
    function(email, password, done) {

        var options = {
            criteria: { email: email },
            select: 'name username email hashed_password salt active'
        };
        Partner.load(options, function(err, partner) {
            if (err) return done(err);

            if (!partner) {
                return done(null, false, {
                    message: 'This email/username is not registered.'
                });
            }
            if (!partner.authenticate(password)) {
                console.log("Wrong password");
                return done(null, false, {
                    message: 'This password is not correct.'
                });
            }
            return done(null, partner);
        });
    }
);