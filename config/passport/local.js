/*!
 * ContoVenditaOnLine
 * Copyright(c) 2015 INNAAS Srl
 * http://www.innaas.com
 */
/**
 * Module dependencies.
 */

var mongoose = require('mongoose');
var LocalStrategy = require('passport-local').Strategy;
var config = require('config');
var User = mongoose.model('User');
var Partner = mongoose.model('Partner');

/**
 * Expose
 */

module.exports = new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password'
  },
  function(email, password, done) {
    var options = {
      criteria: { email: email },
      select: 'name username email hashed_password salt active'
    };
    User.load(options, function (err, user) {
      console.log("load user from local strategy: " + user);
      if (err){
          Partner.load(options, function (err, partner) {
              console.log("load partner from local strategy: " + partner);
              if (err) return done(err)
              if (!partner) {
                  return done(null, false, { message: 'Unknown user' });
              }
              if (!partner.authenticate(password)) {
                  return done(null, false, { message: 'Invalid password' });
              }
//      if (!user.active) {
//            return done(null, false, { message: 'Inactive user' });
//      }
              return done(null, partner);
          });
          return done(err)
      }
      if (!user) {
        return done(null, false, { message: 'Unknown user' });
      }
      if (!user.authenticate(password)) {
        return done(null, false, { message: 'Invalid password' });
      }
//      if (!user.active) {
//            return done(null, false, { message: 'Inactive user' });
//      }
      return done(null, user);
    });
  }
);
