/*!
 * ContoVenditaOnLine
 * Copyright(c) 2015 INNAAS Srl
 * http://www.innaas.com
 */
/**
 * Module dependencies.
 */

var mongoose = require('mongoose');
var Notifier = require('notifier');
var config = require('config');
var util = require('util');

/**
 * Process the templates using swig - refer to notifier#processTemplate method
 *
 * @param {String} tplPath
 * @param {Object} locals
 * @return {String}
 * @api public
 */

Notifier.prototype.processTemplate = function (tplPath, locals) {
  var swig = require('swig');
    console.log("Template path :" +tplPath);
  locals.filename = tplPath;

  return swig.renderFile(tplPath, locals);
};

/**
 * Expose
 */

module.exports = {

  /**
   * Comment notification
   *
   * @param {Object} options
   * @param {Function} cb
   * @api public
   */

  comment: function (options, cb) {
    var article = options.article;
    var author = article.user;
    var user = options.currentUser;
    var notifier = new Notifier(config.notifier);

    var obj = {
      to: author.email,
      from: 'info@cvol.it',
      subject: user.name + ' added a comment on your article ' + article.title,
      alert: user.name + ' says: "' + options.comment,
      locals: {
        to: author.name,
        from: user.name,
        body: options.comment,
        article: article.name
      }
    };

    // for apple push notifications
    //notifier.use({
   //   APN: true,
   //   parseChannels: ['USER_' + author._id.toString()]
   // })

    try {
      //notifier.mail(obj,config.notifier.tplPath + '/comment.html',cb);

      notifier.send('comment', obj, cb);
    } catch (err) {
      console.log("Errore nell'invio mail" + err);
    }
  },


    /**
     * User registration notification
     *
     * @param {Object} options
     * @param {Function} cb
     * @api public
     */

    users: function (options, cb) {
        var user = options.user;
        var notifier = new Notifier(config.notifier);

        var obj = {
            to: user.email,
            from: 'info@cvol.it',
            subject: 'Conferma registrazione ContoVendita Online per '+ user.name,
            alert: user.name + ' s',
            locals: {
                to: user.email,
                from: 'info@cvol.it',
                body: options.user
                //article: article.name
            }

        };
        console.log("mail:  " + util.inspect(obj));
        // for apple push notifications
        //notifier.use({
        //   APN: true,
        //   parseChannels: ['USER_' + author._id.toString()]
        // })

        try {
            notifier.send('users', obj, function(err) {
                //req.flash('info', 'An e-mail has been sent to ' + user.email + ' with further instructions.');
                //done(err, 'done');
                console.log("err " + util.inspect(err))
            });
        } catch (err) {
            console.log("Errore nell'invio mail " + err);
        }
    },


    /**
     * User Forgot Password Mail notification
     *
     * @param {Object} options
     * @param {Function} cb
     * @api public
     */

    usersForgot: function (options, cb) {
        var user = options.user;
        var token = options.token;
        var url = options.url;
        var notifier = new Notifier(config.notifier);
console.log("token : " + token);
        var obj = {
            to: user.email,
            name: user.name,
            from: 'info@cvol.it',
            subject: 'Reset password ContoVendita Online per '+ user.name,
            alert: user.name + ' s',
            locals: {
                to: user.email,
                from: 'info@cvol.it',
                body: options.user,
                token: token,
                url: url
                //article: article.name
            }


        };
        console.log("mail:  " + util.inspect(obj));
        // for apple push notifications
        //notifier.use({
        //   APN: true,
        //   parseChannels: ['USER_' + author._id.toString()]
        // })

        try {
            notifier.send('usersForgot', obj, function(err) {
                //req.flash('info', 'An e-mail has been sent to ' + user.email + ' with further instructions.');
                //done(err, 'done');
                console.log("err " + util.inspect(err))
            });
        } catch (err) {
            console.log("Errore nell'invio mail " + err);
        }
    },

    /**
     * User Change Password Confirm notification
     *
     * @param {Object} options
     * @param {Function} cb
     * @api public
     */

    usersChangePassword: function (options, cb) {
        var user = options.user;
        var url = options.url;
        var notifier = new Notifier(config.notifier);
        var obj = {
            to: user.email,
            name: user.name,
            from: 'info@cvol.it',
            subject: 'Conferma cambio password ContoVendita Online per '+ user.name,
            alert: user.name + ' s',
            locals: {
                to: user.email,
                from: 'info@cvol.it',
                body: options.user,
                url: url
                //article: article.name
            }


        };
        console.log("mail:  " + util.inspect(obj));
        // for apple push notifications
        //notifier.use({
        //   APN: true,
        //   parseChannels: ['USER_' + author._id.toString()]
        // })

        try {
            notifier.send('usersConfirmPasswordChange', obj, function(err) {
                //req.flash('info', 'An e-mail has been sent to ' + user.email + ' with further instructions.');
                //done(err, 'done');
                console.log("err " + util.inspect(err))
            });
        } catch (err) {
            console.log("Errore nell'invio mail " + err);
        }
    },


    partners: function (options, cb) {
        var partner = options.partner;
        var notifier = new Notifier(config.notifier);

        var obj = {
            to: partner.email,
            from: 'info@cvol.it',
            subject: 'Conferma registrazione ContoVendita Online per '+ partner.name,
            alert: partner.name + ' s',
            locals: {
                to: partner.email,
                from: 'info@cvol.it',
                body: options.partner
                //article: article.name
            }

        };
        console.log("dsddsds " + util.inspect(obj));
        // for apple push notifications
        //notifier.use({
        //   APN: true,
        //   parseChannels: ['USER_' + author._id.toString()]
        // })

        try {


            notifier.send('partners', obj, cb);
        } catch (err) {
            console.log("Errore nell'invio mail" + err);
        }
    }


};
