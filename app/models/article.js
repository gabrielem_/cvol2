/*!
 * ContoVenditaOnLine
 * Copyright(c) 2015 INNAAS Srl
 * http://www.innaas.com
 */
/**
 * Module dependencies.
 */

var mongoose = require('mongoose');
var Imager = require('imager');
var config = require('config');
var util = require('util');
var imagerConfig = require(config.root + '/config/imager.js');
var utils = require('../../lib/utils');

var Schema = mongoose.Schema;

/**
 * Getters
 */

var getTags = function (tags) {
  return tags.join(',');
};

/**
 * Setters
 */

var setTags = function (tags) {
  return tags.split(',');
};

/**
 * Article Schema
 */

var ArticleSchema = new Schema({
  type : {type : String, default : '', trim : true},
  make: {type : String, default : '', trim : true},
  model: {type : String, default : '', trim : true},
  name:  {type : String, default : '', trim : true},
  fullName:  {type : String, default : '', trim : true},
  CodiceInfocarAM:  {type : String, default : '', trim : true},
  year: {type : String, default : '', trim : true},
  fuel: {type : String, default : '', trim : true},
  engine: {type : String, default : '', trim : true},
  doors: {type : String, default : '', trim : true},
  seats: {type : String, default : '', trim : true},
  gearbox: {type : String, default : '', trim : true},
  pollution: {type : String, default : '', trim : true},
  description: {type : String, default : '', trim : true},
    Normativa: {type : String, default : '', trim : true},
    Alimentazione: {type : String, default : '', trim : true},
    PotenzaFiscale: {type : String, default : '', trim : true},
    Catalizzata: {type : String, default : '', trim : true},
    TipoCatalizzatore: {type : String, default : '', trim : true},
    MassaKG: {type : String, default : '', trim : true},
    MassaKgDa: {type : String, default : '', trim : true},
    PortataKgDa: {type : String, default : '', trim : true},
    CapacitaBagagliaio1dm: {type : String, default : '', trim : true},
    MassaKgA: {type : String, default : '', trim : true},
    CapacitaBagagliaio2dm: {type : String, default : '', trim : true},
    PortataKgA: {type : String, default : '', trim : true},
    MassaRimorchiabile: {type : String, default : '', trim : true},
    CapacitaBagagliaio3dm: {type : String, default : '', trim : true},
    AltezzaMetri: {type : String, default : '', trim : true},
    NumeroPorte: {type : String, default : '', trim : true},
    Carrozzeria: {type : String, default : '', trim : true},
    NumeroPosti: {type : String, default : '', trim : true},
    PassoMetri: {type : String, default : '', trim : true},
    LunghezzaMetri: {type : String, default : '', trim : true},
    DescrizioneCompleta: {type : String, default : '', trim : true},
    PotenzaCV: {type : String, default : '', trim : true},
    CapacitaSerbatoioL: {type : String, default : '', trim : true},
    Cambio: {type : String, default : '', trim : true},
    Accelerazione0100: {type : String, default : '', trim : true},
    VelocitaMax: {type : String, default : '', trim : true},
    Cilindrata: {type : String, default : '', trim : true},
    NumeroMarce: {type : String, default : '', trim : true},
    ValvolePerCilindro: {type : String, default : '', trim : true},
    NumeroCilindri: {type : String, default : '', trim : true},
    PotenzaKW: {type : String, default : '', trim : true},
    CoppiaKgm: {type : String, default : '', trim : true},
    TipoConsumo: {type : String, default : '', trim : true},
    ConsumoKg2: {type : String, default : '', trim : true},
    Consumo3Stimato: {type : String, default : '', trim : true},
    DisposizioneCilindri: {type : String, default : '', trim : true},
    Consumo3: {type : String, default : '', trim : true},
    CoppiaMaxGiriMinuto: {type : String, default : '', trim : true},
    RapportoPotenzaMassima: {type : String, default : '', trim : true},
    Sovralimentazione: {type : String, default : '', trim : true},
    Emissione2: {type : String, default : '', trim : true},
    AutonomiaVelCostante: {type : String, default : '', trim : true},
    Consumo2: {type : String, default : '', trim : true},
    ConsumoKg3: {type : String, default : '', trim : true},
    ConsumoKg1: {type : String, default : '', trim : true},
    Emissione3: {type : String, default : '', trim : true},
    VelCostante: {type : String, default : '', trim : true},
    PotenzaMaxGiriMinuto: {type : String, default : '', trim : true},
    AutonomiaUrbana: {type : String, default : '', trim : true},
    CoppiaNm: {type : String, default : '', trim : true},
    Consumo1: {type : String, default : '', trim : true},
    Emissione1: {type : String, default : '', trim : true},
    Trazione: {type : String, default : '', trim : true},
    CapacitaSerbatoioKg: {type : String, default : '', trim : true},


  user: {type : Schema.ObjectId, ref : 'User'},
  price: {type: Number, get: getPrice, set: setPrice },
  comments: [{
    body: { type : String, default : '' },
    user: { type : Schema.ObjectId, ref : 'User' },
    createdAt: { type : Date, default : Date.now }
  }],
  tags: {type: [], get: getTags, set: setTags},
  image: {
    cdnUri: String,
    path_id: String,
    files: []
  },
  createdAt  : {type : Date, default : Date.now},
  city:{type : String, default : '', trim : true},
  cityCode:{type : Number, default : ''},
  lat:{type : String, default : '', trim : true},
  lon:{type : String, default : '', trim : true},
  prov:{type : Number, default : ''},
  cap:{type : Number, default : ''},
  status:{type : Number, default : 10},
  km:{type : Number, default : '',get: getKm},
  color:{type : String, default : ''}
});

/**
 * Validations
 */
ArticleSchema.path('type').required(true, 'Il campo Type non può essere vuoto');
ArticleSchema.path('make').required(true, 'Il campo Produttore non può essere vuoto');
ArticleSchema.path('model').required(true, 'Il campo Modello non può essere vuoto');
ArticleSchema.path('description').required(true, 'Il campo Descrizione non può essere vuoto');
ArticleSchema.path('price').required(true, 'Il campo Prezzo non può essere vuoto');
ArticleSchema.path('year').required(true, 'Il campo Anno non può essere vuoto');



function getPrice(num){

    var digitsRegex= /(\d{3})(?=\d)/g;


    input = parseFloat(num);

    if(!isFinite(input) || (!input && input !== 0)) {
        return '';
    }


    var strVal = Math.floor(Math.abs(input)).toString();
    var i = strVal.length % 3;

        var h = i > 0 ?
            (strVal.slice(0, i) + (strVal.length > 3 ? '.' : '')) :
            '';
        var v = Math.abs(parseInt((input * 100) % 100, 10));
        return (input < 0 ? '-' : '') +
            h + strVal.slice(i).replace(digitsRegex, '€1,');
}

function setPrice(num){

    return num;
}

function getKm(num){

    var digitsRegex= /(\d{3})(?=\d)/g;


    input = parseFloat(num);

    if(!isFinite(input) || (!input && input !== 0)) {
        return '';
    }


    var strVal = Math.floor(Math.abs(input)).toString();
    var i = strVal.length % 3;

    var h = i > 0 ?
        (strVal.slice(0, i) + (strVal.length > 3 ? '.' : '')) : '';
    var v = Math.abs(parseInt((input * 100) % 100, 10));

    return (input < 0 ? '-' : '') +
        h + strVal.slice(i).replace(digitsRegex, '1');
}
/**
 * Pre-remove hook
 */

ArticleSchema.pre('remove', function (next) {
  var imager = new Imager(imagerConfig.variants.article, imagerConfig.storage.S3);
  var files = this.image.files;

  // if there are files associated with the item, remove from the cloud too
  imager.remove(files, function (err) {
    if (err) return next(err);
  });

  next();
});

/**
 * Methods
 */

ArticleSchema.methods = {

  /**
   * Save article and upload image
   *
   * @param {Object} images
   * @param {Function} cb
   * @api private
   */

  uploadAndSave: function (images, cb) {
    if (!images || !images.length) return this.save(cb)

    var imager = new Imager(imagerConfig.variants.article, imagerConfig.storage.S3);
    var self = this;


      self.image = { cdnUri : imagerConfig.storage.S3.cdnUri, files : images};

    this.validate(function (err) {
      if (err) return cb(err);
      imager.upload(images, function (err, cdnUri, images) {
        if (err) return cb(err);
//        if (images.length) {

//        }
        self.save(cb);
      });
    });
  },

  /**
   * Add comment
   *
   * @param {User} user
   * @param {Object} comment
   * @param {Function} cb
   * @api private
   */

  addComment: function (user, comment, cb) {
    var notify = require('../mailer');

    this.comments.push({
      body: comment.body,
      user: user._id
    });

    if (!this.user.email) this.user.email = 'email@product.com';
    notify.comment({
      article: this,
      currentUser: user,
      comment: comment.body
    });

    this.save(cb);
  },

  /**
   * Remove comment
   *
   * @param {commentId} String
   * @param {Function} cb
   * @api private
   */

  removeComment: function (commentId, cb) {
    var index = utils.indexof(this.comments, { id: commentId });
    if (~index) this.comments.splice(index, 1);
    else return cb('not found');
    this.save(cb);
  },

/**
 * Remove image
 *
 * @param {imageIndex} Int
 * @param {Function} cb
 * @api private
 */

  removeImg: function (imageIndex, cb) {
    var images = this.image.files;
    if (!imageIndex) return cb('not found');
    images.splice(imageIndex, 1);
    console.log("new array " + images);
    this.save(cb);
  }
}

/**
 * Statics
 */

ArticleSchema.statics = {

  /**
   * Find article by id
   *
   * @param {ObjectId} id
   * @param {Function} cb
   * @api private
   */

  load: function (id, cb) {
    this.findOne({ _id : id })
      .populate('user', 'name email username')
      .populate('comments.user')
      .exec(cb);
  },

  /**
   * List articles
   *
   * @param {Object} options
   * @param {Function} cb
   * @api private
   */

  list: function (options, cb) {
    var criteria = options.criteria || {}

    this.find(criteria)
     // .populate('user', 'name username')
      .sort({'createdAt': -1}) // sort by date
      .limit(options.perPage)
      .skip(options.perPage * options.page)
      .exec(cb);
  },

    /**
     * Count articles
     *
     * @param {Object} options
     * @param {Function} cb
     * @api private
     */

    countArticles: function (options, cb) {
        var criteria = options.criteria || {}

        this.count(criteria)
            .exec(cb);
    }
}

mongoose.model('Article', ArticleSchema);
