/*!
 * ContoVenditaOnLine
 * Copyright(c) 2015 INNAAS Srl
 * http://www.innaas.com
 */
/**
 * Module dependencies.
 */

var mongoose = require('mongoose');
var crypto = require('crypto');
var Imager = require('imager');
var config = require('config');
var util = require('util');
var imagerConfig = require(config.root + '/config/imager.js');
var Schema = mongoose.Schema;


/**
 * User Schema
 */

var PartnerSchema = new Schema({
  name: { type: String, default: '' },
  piva: {type: Number, default: 0},
  email: { type: String, default: '' },
  username: { type: String, default: '' },
  provider: { type: String, default: '' },
  active: {type: Boolean, default: false},
  hashed_password: { type: String, default: '' },
  salt: { type: String, default: '' },
  authToken: { type: String, default: '' },
  cell: {type: String, default: ''},
  smscount: {type: Number, default: 0},
  code: { type: String, default: '' },
  cap: { type: String},
  address: { type: String, default: '' },
  website: { type: String, default: '' },
  skype: { type: String, default: '' },
  city:{type : String, default : '', trim : true},
  cityCode:{type : Number, default : ''},
  lat:{type : String, default : '', trim : true},
  lon:{type : String, default : '', trim : true},
  prov:{type : Number, default : ''},
  resetPasswordToken: String,
  resetPasswordExpires: Date,
    image: {
        cdnUri: String,
        path_id: String,
        files: []
    }
});

/**
 * Virtuals
 */

PartnerSchema
  .virtual('password')
  .set(function(password) {
    this._password = password;
    this.salt = this.makeSalt();
    this.hashed_password = this.encryptPassword(password);
  })
  .get(function() { return this._password });

/**
 * Validations
 */

var validatePresenceOf = function (value) {
  return value && value.length;
};

// the below 5 validations only apply if you are signing up traditionally

PartnerSchema.path('name').validate(function (name) {
  if (this.skipValidation()) return true;
  return name.length;
}, 'Name cannot be blank');

PartnerSchema.path('email').validate(function (email) {
  if (this.skipValidation()) return true;
  return email.length;
}, 'Email cannot be blank');

PartnerSchema.path('email').validate(function (email, fn) {
  var Partner = mongoose.model('Partner');
  if (this.skipValidation()) fn(true);

  // Check only when it is a new user or when email field is modified
  if (this.isNew || this.isModified('email')) {
    Partner.find({ email: email }).exec(function (err, partners) {
      fn(!err && partners.length === 0);
    });
  } else fn(true);
}, 'Email already exists');

PartnerSchema.path('username').validate(function (username) {
  if (this.skipValidation()) return true;
  return username.length;
}, 'Username cannot be blank');

PartnerSchema.path('hashed_password').validate(function (hashed_password) {
  if (this.skipValidation()) return true;
  return hashed_password.length;
}, 'Password cannot be blank');


/**
 * Pre-save hook
 */

PartnerSchema.pre('save', function(next) {
  if (!this.isNew) return next();

  if (!validatePresenceOf(this.password) && !this.skipValidation()) {
    next(new Error('Invalid password'));
  } else {
    next();
  }
})

/**
 * Methods
 */

PartnerSchema.methods = {

  /**
   * Authenticate - check if the passwords are the same
   *
   * @param {String} plainText
   * @return {Boolean}
   * @api public
   */

  authenticate: function (plainText) {
    return this.encryptPassword(plainText) === this.hashed_password;
  },

  /**
   * Make salt
   *
   * @return {String}
   * @api public
   */

  makeSalt: function () {
    return Math.round((new Date().valueOf() * Math.random())) + '';
  },

  /**
   * Encrypt password
   *
   * @param {String} password
   * @return {String}
   * @api public
   */

  encryptPassword: function (password) {
    if (!password) return '';
    try {
      return crypto
        .createHmac('sha1', this.salt)
        .update(password)
        .digest('hex');
    } catch (err) {
      return '';
    }
  },

  /**
   * Validation is not required if using OAuth
   */

  skipValidation: function() {
    //return ~oAuthTypes.indexOf(this.provider);
  },

    /**
     * Save partner data and upload image
     *
     * @param {Object} images
     * @param {Function} cb
     * @api private
     */

    uploadAndSave: function (images, cb) {
        console.log(util.inspect(images));
        if (!images || !images.length) return this.save(cb)

        var imager = new Imager(imagerConfig.variants.partner, imagerConfig.storage.S3);
        var self = this;


        self.image = { cdnUri : imagerConfig.storage.S3.cdnUri, files : images};

        this.validate(function (err) {
            if (err) return cb(err);
            imager.upload(images, function (err, cdnUri, images) {
                if (err) return cb(err);
//        if (images.length) {

//        }
                self.save(cb);
            });
        });
    }
};

/**
 * Statics
 */

PartnerSchema.statics = {

  /**
   * Load
   *
   * @param {Object} options
   * @param {Function} cb
   * @api private
   */

  load: function (options, cb) {
    options.select = options.select || 'name email username cell piva active code smscount address city cap skype website piva image';
    this.findOne(options.criteria)
      .select(options.select)
      .exec(cb);
  }
}

mongoose.model('Partner', PartnerSchema);
