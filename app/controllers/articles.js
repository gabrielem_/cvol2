/*!
 * ContoVenditaOnLine
 * Copyright(c) 2015 INNAAS Srl
 * http://www.innaas.com
 */
/**
 * Module dependencies.
 */

var mongoose = require('mongoose')
var Article = mongoose.model('Article')
var utils = require('../../lib/utils')
var extend = require('util')._extend
var util = require('util');
var http = require('http');
var querystring = require('querystring');
var host = 'www.infocar.org';
var account = {Login:"DAT-infHYS",Password:"Incontro2015!"};
var ObjectId = require('mongoose').Types.ObjectId;
/**
 * Load
 */

exports.load = function (req, res, next, id){
  var User = mongoose.model('User');

  Article.load(id, function (err, article) {
    if (err) return next(err);
    if (!article) return next(new Error('not found'));
    req.article = article;
    next();
  });
};

/**
 * List
 */

exports.index = function (req, res){
  var page = (req.param('page') > 0 ? req.param('page') : 1) - 1;
  var perPage = 10;
  var options = {
    perPage: perPage,
    page: page
  };


  Article.list(options, function (err, articles) {
    if (err) return res.render('500');
    Article.count().exec(function (err, count) {
      res.render('articles/index', {
        title: 'Articles',
        articles: articles,
        page: page + 1,
        pages: Math.ceil(count / perPage)
      });
        console.log("articles :" + util.inspect(articles));
    });
  });
};

/**
 * List users Articles
 */

exports.userArticles = function (userid, req, res, next) {
    var query = {'user': new ObjectId(req.profile.id)}
    var options = {};
    options.criteria = query;
    //if (err) return res.render('500');
    console.log('LIST ARTICLES for ' + req.profile.id);
    Article.list(options, function (err, articles) {
        if (err) return res.render('500');
       // res.articles = articles;
        console.log("res.articles = " + articles);
        return next(articles);
            });

};

/**
 * List Articles in status 10 for Partners
 */

exports.partnersListNewArticles = function (req, res){
    var page = (req.param('page') > 0 ? req.param('page') : 1) - 1;
    var perPage = 10;
    var options = {
        perPage: perPage,
        page: page
    };
    var query = {'status': { $gte: 10 }};
    options.criteria = query;
console.log("ENTER");
    Article.list(options, function (err, articles) {
        if (err) return res.render('500');
        Article.find(query).count(options).exec(function (err, count) {
            res.render('partners/newArticles', {
                //title: 'Articles',
                articles: articles,
                page: page + 1,
                pages: Math.ceil(count / perPage)
            });
            console.log("articles :" + util.inspect(articles));
        });
    });
};

/**
 * New article
 */

exports.new = function (req, res){
  res.render('articles/new', {
    title: 'New Article',
    article: new Article({})
  });
};

/**
 * Create an article
 * Upload an image
 */

exports.create = function (req, res) {
  var article = new Article(req.body);
    var images = req.files.image
        ? req.files.image
        : undefined;


  if(images)  images.path_id = article.id;

  article.user = req.user;
  article.uploadAndSave(images, function (err) {
    if (!err) {
      req.flash('success', 'Successfully created article!');
      return res.redirect('/articles/'+article._id);
    }

      console.log('There was something')
      console.log(err);
    res.render('articles/new', {
      title: 'New Article',
      article: article,
      errors: utils.errors(err.errors || err)
    });
  });
};

/**
 * Edit an article
 */

exports.edit = function (req, res) {
  res.render('articles/edit', {
    title: 'Edit ' + req.article.title,
    article: req.article
  });
};


/**
 * Edit image article
 */

exports.removeImage = function (req, res) {
    console.log ( "IMAGES : " ) ;
var images = req.article.images;
    console.log ( "IMAGES : " + util.inspect(req.article.image)) ;
    Article.removeImg(1, function(err){
        console.log ( "IMAGES : " + util.inspect(req.article.image)) ;
    });
};
//TODO manage image substitution
/**
 * Update article
 */

exports.update = function (req, res){
  var article = req.article;
  var images = req.files.image
    ? [req.files.image]
    : undefined;

  // make sure no one changes the user
  delete req.body.user;
  article = extend(article, req.body);
  console.log ( "IMAGES : " + util.inspect(req.article.image)) ;
    console.log ( "NEW IMAGES : " + util.inspect(req.files.image)) ;

  article.uploadAndSave(images, function (err) {
    if (!err) {
      return res.redirect('/articles/' + article._id);
    }

    res.render('articles/edit', {
      title: 'Edit Article',
      article: article,
      errors: utils.errors(err.errors || err)
    });
  });
};

/**
 * Show
 */

exports.show = function (req, res){
  res.render('articles/show', {
    title: req.article.title,
    article: req.article
  });
};

/**
 * Delete an article
 */

exports.destroy = function (req, res){
  var article = req.article;
  article.remove(function (err){
    req.flash('info', 'Deleted successfully');
    res.redirect('/articles');
  });
};

/**
 * Query
 */

exports.query = function (req, res){
    var page = (req.param('page') > 0 ? req.param('page') : 1) - 1;
    var perPage = 10;
    var options = {
        perPage: perPage,
        page: page
    };


    //build string
    var query = {}

    if ((req.param('price1') != '' || req.param('price2') != '' || req.param('year1') != '' || req.param('year2') != '' || req.param('Km1') != '' || req.param('Km2') != '' || req.param('photo') == 'on'))query["$and"] = [];

    if (req.param('price1') != '') query["$and"].push({"price": { $gte: req.param('price1')}});

    if (req.param('price2') != '') query["$and"].push({"price": { $lte: req.param('price2')}});

    if (req.param('year1') != '') query["$and"].push({"year": { $gte: req.param('year1')}});

    if (req.param('year2') != '') query["$and"].push({"year": { $lte: req.param('year2')}});

    if (req.param('Km1') != '') query["$and"].push({"km": { $gte: req.param('Km1')}});

    if (req.param('Km2') != '') query["$and"].push({"km": { $lte: req.param('Km2')}});

    if (req.param('fuel') != '0') query["fuel"] = req.param('fuel');

    if (req.param('marca')) query["make"] = req.param('marca');

    if (req.param('prov')) query["prov"] = req.param('prov');

    if (req.param('photo') == 'on') query["$and"].push({image: {$exists: true}, $where: 'this.image.files.length>0'});


    console.log("Query: " + util.inspect(query));

    options.criteria = query;

    Article.list(options, function (err, articles) {
        if (err) return res.render('500');
        Article.find(query).count(function (err, count) {
            res.render('articles/index', {
                title: 'Articles',
                articles: articles,
                page: page + 1,
                pages: Math.ceil(count / perPage),
                count: count
            });
            console.log("articles :" + page +" & "+ perPage + " & " + Math.ceil(count / perPage) + " count: " +count);
        });
    });

},


exports.loadModels = function(req, res){
    var parameters = {CodiceMarca:req.query.marca, ImmatricolazioneDa :req.query.year,ImmatricolazioneA  :req.query.year, Alimentazione: req.query.fuel};
    console.log("PARAMETERS :" + util.inspect(parameters));

    performRequest('/QPServices/Services/ADSQPService.svc/rest/GetModelli', 'POST', {
        credenziali: account,
        parametro: parameters
    }, function(data) {

        res.writeHead(200, {
            'Content-Type': 'application/json'
        });
        res.end(JSON.stringify(data));

    });

};

exports.loadCar = function(req, res){
    var parameters2 = {CodiceModello:req.query.modello, ImmatricolazioneDa :req.query.year,ImmatricolazioneA  :req.query.year, Alimentazione: req.query.fuel};
    console.log("PARAMETERS :" + util.inspect(parameters2));

    performRequest('/QPServices/Services/ADSQPService.svc/rest/GetAllestimenti', 'POST', {
        credenziali: account,
        parametro: parameters2
    }, function(data) {

        res.writeHead(200, {
            'Content-Type': 'application/json'
        });
        res.end(JSON.stringify(data));

    });

};
//Load Car Data
exports.loadData = function(req, res){
    var parameters3 = {CodiceInfocarAM:req.query.car};
    console.log("PARAMETERS :" + util.inspect(parameters3));

    performRequest('/QPServices/Services/ADSQPService.svc/rest/GetEquipNormalizzati', 'POST', {
        credenziali: account,
        parametro: parameters3
    }, function(data) {

        res.writeHead(200, {
            'Content-Type': 'application/json'
        });
        res.end(JSON.stringify(data));

    });

};
//LOAD CAR  TECH DATA
exports.loadTechData = function(req, res){
    var parameters4 = {CodiceInfocarAM:req.query.car};
    console.log("PARAMETERS :" + util.inspect(parameters4));

    performRequest('/QPServices/Services/ADSQPService.svc/rest/GetDatiTecniciDettagliati', 'POST', {
        credenziali: account,
        parametro: parameters4
    }, function(data) {

        res.writeHead(200, {
            'Content-Type': 'application/json'
        });
        res.end(JSON.stringify(data));

    });

};


function performRequest(endpoint, method, data, success) {
    var dataString = JSON.stringify(data);
    var headers = {};

    if (method == 'GET') {
        endpoint += '?' + querystring.stringify(data);
    }
    else {
        headers = {
            'Content-Type': 'application/json',
            'Content-Length': dataString.length
        };
    }
    var options = {
        host: host,
        path: endpoint,
        method: method,
        headers: headers
    };

    var req = http.request(options, function(res) {
        res.setEncoding('utf-8');

        var responseString = '';

        res.on('data', function(data) {
            responseString += data;
        });

        res.on('end', function() {

            var responseObject = JSON.parse(responseString);
            success(responseObject);
        });
    });

    req.write(dataString);
    req.end();
}
