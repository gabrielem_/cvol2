/*!
 * ContoVenditaOnLine
 * Copyright(c) 2015 INNAAS Srl
 * http://www.innaas.com
 */

/**
 * Module dependencies.
 */

var mongoose = require('mongoose');
var Partner = mongoose.model('Partner');
var utils = require('../../lib/utils');
var util = require('util');
/**
 * Load
 */

exports.load = function (req, res, next, id) {
  var options = {
    criteria: { _id : id }
  };
  Partner.load(options, function (err, partner) {
    if (err) return next(err);
    if (!partner) return next(new Error('Failed to load User ' + id));
    req.profile = partner;
    req.session.p = true;
      console.log("profile " + util.inspect(req.session));
    next();
  });
};

/**
 * Create partner
 */

exports.create = function (req, res) {
  var partner = new Partner(req.body);
  var notify = require('../mailer');
    partner.provider = 'local';
    var images = req.files.image
        ? req.files.image
        : undefined;

console.log("image: " +util.inspect(req.files.image));
    if(images)  images.path_id = partner.id;
    partner.uploadAndSave(images, function (err) {
    if (err) {
      return res.render('partners/signup', {
        error: utils.errors(err.errors),
        partner: partner,
        title: 'Sign up'
      });
    }
      notify.partners({
          partner: partner
      });
    // manually login the user once successfully signed up
    req.logIn(partner, function(err) {
      if (err) req.flash('info', 'Sorry! We are not able to log you in!');
      return res.redirect('/partners/' + req.user.id);
    });
  });
};

/**
 *  Show profile
 */

exports.show = function (req, res) {
  var partner = req.profile;
  console.log("partner: " + partner);
  res.render('partners/show', {
    title: partner.name,
      partner: partner
  });
};

exports.signin = function (req, res) {};

/**
 * Auth callback
 */

exports.authCallback = login;

/**
 * Show login form
 */

exports.login = function (req, res) {
  res.render('partners/login', {
    title: 'Login'
  });
};

/**
 * Show sign up form
 */

exports.signup = function (req, res) {
  res.render('partners/signup', {
    title: 'Sign up',
      partner: new Partner()
  });
};

/**
 * Logout
 */

exports.logout = function (req, res) {

    req.logout();
    req.session.destroy();
    req.session = null;
  res.redirect('partners/login');
};

/**
 * Session
 */

exports.session = login;

/**
 * Login
 */

function login (req, res) {
  var redirectTo = req.session.returnTo ? req.session.returnTo : '/partners/' + req.user.id;
  delete req.session.returnTo;
    console.log("session: " + util.inspect(req.session.passport));
  res.redirect(redirectTo);
};
