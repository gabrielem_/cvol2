/*!
 * ContoVenditaOnLine
 * Copyright(c) 2015 INNAAS Srl
 * http://www.innaas.com
 */

/**
 * Module dependencies.
 */

var mongoose = require('mongoose');
var User = mongoose.model('User');
var util = require('util');
var utils = require('../../lib/utils');
var extend = require('util')._extend;
var recaptcha = require('node-no-captcha');
var secret = '6LcTCQgTAAAAADmSPVbA8HvbNVwUtgv117NpI6uk';
var async = require('async');
var crypto = require('crypto');
var articles = require('../controllers/articles');





exports.forgot = function (req, res) {
    console.log("ENTER FORGOT");
    res.render('users/forgot');
};

/**
 * Load
 */

exports.load = function (req, res, next, id) {
    console.log("profile " + util.inspect(req.session));
  var options = {
    criteria: { _id : id }
  };
  User.load(options, function (err, user) {
    if (err) return next(err);
    if (!user) return next(new Error('Failed to load User ' + id));
    req.profile = user;
    console.log("profile " + util.inspect(req.session));
    next();
  });
};
/**
 *
 */

/**
 * Create user
 */

exports.create = function (req, res, next) {
  var user = new User(req.body);
  var notify = require('../mailer');
  user.provider = 'local';
console.log("enter");
    if(req.body['g-recaptcha-response'] ){
    console.log("captcha verified");
    user.save(function (err) {
    if (err) {
        console.log("errors " + util.inspect(err.errors));
        return res.render('users/signup', {
        errors: utils.errors(err.errors || err),
        user: user,
        title: 'Sign up'
      });
    }
      console.log("start email function");
        notify.users({
          user: user
      });

//Send activation code via SMS
      var message = "Benvenuto su ContoVendita OnLine " + user.name + "! Il tuo codice di attivazione è il seguente: " +user.code;
      var recipient = [user.cell]
    //sendSMS(user, message, recipient);

    // manually login the user once successfully signed up
    req.logIn(user, function(err) {
      if (err) req.flash('info', 'Sorry! We are not able to log you in!');
      //add redirect to user activation page
      //generate a custom activation code sent by sms
      //using last 4 salt digits
      res.profile = user;
      return res.render('users/activation',{
          title: user.name,
          user: user
      });
    });
  });
 }else{
        req.flash('error', 'Attenzione! E\' necessario il flag di verifica Captcha prima di proseguire');
        console.log('false captured');
        return res.render('users/signup', {
            //message : ('error', 'Attenzione! E\' necessario il flag di verifica Captcha prima di proseguire'),
            user: user,
            title: 'Sign up'
        });
    }
};

/**
 *  Show profile
 */

exports.show = function (req, res) {
  var user = req.profile;
articles.userArticles(user.id, req, res, function( articles ){

    req.articles = articles;

    res.render('users/show', {
        title: user.name,
        articles: req.articles,
        user: user
    });

  });


};

exports.signin = function (req, res) {};

/**
 * Auth callback
 */

exports.authCallback = login;

/**
 * Show login form
 */

exports.login = function (req, res) {
  res.render('users/login', {
    title: 'Login'
  });
};

/**
 * Show sign up form
 */

exports.signup = function (req, res) {
  res.render('users/signup', {
    title: 'Sign up',
    user: new User()
  });
};

/**
 * Logout
 */

exports.logout = function (req, res) {
  req.logout();
  res.redirect('/login');
};

/**
 * Session
 */

exports.session = login;





/**
 * Login
 */

function login (req, res) {
  var redirectTo = req.session.returnTo ? req.session.returnTo : '/';
  delete req.session.returnTo;
    console.log("profile " + util.inspect(req.session));
  res.redirect(redirectTo);
};

exports.activate = function (req, res, next) {
    var us = req.body;
    var user;
    var options = {
        criteria: { _id : us.id },
        select: 'code active'
    };

    User.load(options, function (err, user) {
        if (err) return next(err);
        if (!user) return next(new Error('Failed to load User ' + id));
        req.user = user;
    });



console.log("req.profile :" + util.inspect(req.user));
   // console.log("req.body :" + util.inspect(req.body));


//    {
//        if (err) return (err);
//        if (!us) next(new Error('Failed to load User ' + id));
console.log("Inserted code is: " + us.code + " and expected code is: " + req.user.code);
        if(us.code == req.user.code) {
            console.log("Attivazione in corso");
            User.activateUser(us.id, true, function (req, res) {
                console.log("Utente attivato " +us.id);
            });
            req.flash('success', 'Complimenti! Il tuo account adesso è attivo, da questo momento potrai inserire il tuo annuncio');
        }
        if(us.code != req.user.code){
            User.activateUser(us.id, false, function () {
                console.log("Utente non attivato " +us.id);
            });
            req.flash('error', 'Attenzione! Il codice inserito non è corretto');
        }
//    });
        res.redirect('/users/' + us.id);

};


exports.forgotMail = function(req, res, next) {
    async.waterfall([
        function(done) {
            crypto.randomBytes(20, function(err, buf) {
                var token = buf.toString('hex');
                done(err, token);
            });
        },
        function(token, done) {
            User.findOne({ email: req.body.email }, function(err, user) {
                if (!user) {
                    req.flash('error', 'Account non trovato, verifica l\'indirizzo e-mail inserito');
                    return res.redirect('/users/forgot');
                }
                if(user.provider === 'google'){
                    req.flash('error', 'L\'account è registrato come Google account, prova ad accedere usando il tasto "Login con Google+"');
                    return res.redirect('/users/forgot');
                }
                if(user.provider === 'facebook'){
                    req.flash('error', 'L\'account è registrato come Facebook account, prova ad accedere usando il tasto "Login con Facebook"');
                    return res.redirect('/users/forgot');
                }
                user.resetPasswordToken = token;
                user.resetPasswordExpires = Date.now() + 3600000; // 1 hour

                user.save(function(err) {
                    done(err, token, user);
                });
            });
        },
        function(token, user, done) {
            var notify = require('../mailer');
            console.log("URL : "+ req.headers.host)  ;
            console.log("URL : "+ token)  ;
            notify.usersForgot({user: user, token: token, url: req.headers.host}) ;
            req.flash('info', 'Riceverai a breve una mail all\'indirizzo '+ user.email +' con le istruzioni per poter inserire una nuova password');
            return res.redirect('/users/forgot');
        }
    ], function(err) {
        if (err) return next(err);
        res.redirect('/users/forgot');
    });
};

exports.resetPassword = function(req, res) {
    User.findOne({ resetPasswordToken: req.params.token, resetPasswordExpires: { $gt: Date.now() } }, function(err, user) {
        if (!user) {
            req.flash('error', 'Password reset token non valido o scaduto.');
            return res.redirect('/users/forgot');
        }
        res.render('users/reset', {
            user: req.user,
            token:req.params.token
        });
    });
};

exports.resetPasswordAction = function(req, res) {
    async.waterfall([
        function(done) {
            console.log("Start ");
            User.load({ resetPasswordToken: req.params.token, resetPasswordExpires: { $gt: Date.now() } }, function(err, user) {
                if (!user) {
                    console.log("token " +req.params.token);
                    req.flash('error', 'Password reset token non valido o scaduto.');
                    return res.redirect('back');
                }

                user.password = req.body.password;
                user.resetPasswordToken = undefined;
                user.resetPasswordExpires = undefined;
                console.log("user  " +req.body.password);
                user.save(function(err) {
                    req.logIn(user, function(err) {
                        done(err, user);
                    });
                });
            });
        },
        function(user, done) {
            var notify = require('../mailer');
            notify.usersChangePassword({user: user, url:req.headers.host});
                req.flash('info', 'Grazie ' + user.name + ' adesso potrai accedere utilizzando la nuova password!.');
                return res.redirect('/login');
        }
    ], function(err) {
        res.redirect('/');
    });
};


exports.activation = function (req, res) {
    res.render('users/activation', {
        title: 'activation',
        user: req.user
    });

};




exports.sendMessage = function (req, res) {
    console.log("ENTER " + req.body.id);
    var options = {
        criteria: { _id : req.body.id },
        select: 'cell smscount code name'
    };
    User.load(options, function (err, us) {
        if (err) return (err);
        if (!us) {
            req.flash('error', 'Utente non trovato');
        };

        console.log('User phone is ' + req.body.cell );

        if(us.smscount > 10) {
            req.flash('error', 'Numero massimo di SMS superato');
        }
        if(req.body.cell){

            us.cell = req.body.cell;
            us.smscount = us.smscount + 1;

            us.save();
            console.log("User phone saved " + us);
//Send activation code via SMS
            var message = "Benvenuto su ContoVendita OnLine " + us.name + "! Il tuo codice di attivazione è il seguente: " + us.code;
            var recipient = ['39' + req.body.cell]
            console.log("Messaggio " + message);
            sendSMS(us.name, message, recipient);
            console.log("Sending sms to " + recipient);
            console.log("Sending sms text " + message);
            req.flash('success', 'SMS inviato correttamente al numero: ' + recipient);
            res.redirect('/users/' + req.body.id);
        }
        // TODO: fix the code in sms text
    });


};





function sendSMS(user, message, recipient){
    var qs = require('querystring');
    var https = require('https');

    var send_sms_skebby = function(input,cb,cb_err){
        var text = message;
        var sender_number = input.sender_number || "";
        var sender_string = input.sender_string || "CVOL.IT";
        var method = input.method;
        var lrecipients = input.recipients || [];
        var username = input.username;
        var password = input.password;

        if(!method){
            cb_err("No Method!");
            return;
        }

        switch(method) {
            case 'classic':
                method='send_sms_classic';
                break;
            case 'report':
                method='send_sms_classic_report';
                break;
            case 'basic':
            default:
                method='send_sms_basic';
        }

        var test = input.test || false;

        // Check params
        if(lrecipients.length == 0){
            cb_err("No recipient!");
            return;
        }

        if(!sender_string && !sender_number){
            cb_err("No sender!");
            return;
        }

        if(!text){
            cb_err("No text!");
            return;
        }

        var params = {
            method : method,
            username : username,
            password : password,
            "recipients[]" : lrecipients,
            text : text,
            charset : "UTF-8"
        };

        if(sender_number){
            params.sender_number = sender_number;
        }
        else if(sender_string){
            params.sender_string = sender_string;
        }

        if(test){
            params.method = "test_"+params.method;
        }

        var res_done = false;
        var data = qs.stringify(params);

        var client = https.request({
            port : 443,
            path : "/api/send/smseasy/advanced/http.php",
            host: "gateway.skebby.it",
            method : "POST",
            headers: {
                "Content-Type" : "application/x-www-form-urlencoded",
                "Content-Length": data.length,
                "Content-Encoding" : "utf8"
            }
        },function(res){
            var res_data = "";
            res.on('data', function(data) {
                res_data+=data;
            });
            res.on("end",function(){
                if (!res_done){
                    var res_parsed = qs.parse(res_data);
                    if(res_parsed.status == "success"){
                        cb({data:res_parsed});
                    }
                    else{
                        cb_err(res_parsed);
                    }
                    res_done = true;
                }
            });
        });

        client.end(data);
        client.on('error', function(e) {
            if (!res_done){
                cb_err(e);
                res_done = true;
            }
        });
    };

// SMS CLASSIC dispatch



    send_sms_skebby( {
        method : "classic",
        username : "cvol",
        password : "motoincontro",
        recipients : recipient || [user.cell],
        //recipients : ["393396803445","393395352490"],
        text : message
    },function(res){
        console.log(res.data);
    },function(err){
        console.log(err);
    });

}

